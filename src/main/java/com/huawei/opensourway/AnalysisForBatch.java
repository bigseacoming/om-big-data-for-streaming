package com.huawei.opensourway;

import Utils.HttpClientUtils;
import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Iterator;
import java.util.Properties;

public class AnalysisForBatch {
    public static ObjectMapper objectMapper = new ObjectMapper();
    private static SparkConf sparkConf;
    private static String scheme;
    private static String host;
    private static String port;
    private static String vhost;
    private static URI uri;
    static Properties conf;
    static Logger logger;

    static {
         conf = new Properties();
        try {
            Init Init = new Init();
            InputStream resourceAsStream = Init.class.getResourceAsStream("/resources/conf.properties");
            conf.load(resourceAsStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        vhost = String.valueOf(conf.get("vhost"));
        scheme = String.valueOf(conf.get("es.scheme"));
        host = String.valueOf(conf.get("es.host"));
        port = String.valueOf(conf.get("es.port"));
        try {
            uri = new URIBuilder().setScheme(scheme).setHost(host).setPort(Integer.parseInt(port)).setPath("/_bulk").build();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

    }


    public static void main(String[] args)  {
        String taskCount = args[2];
        String inputPath = args[3];
        SparkConf sparkConf = getSparkConf(args);
        JavaSparkContext jsc = new JavaSparkContext(sparkConf);
        JavaRDD<String> srcRDD = jsc.textFile(inputPath, Integer.parseInt(taskCount));
        JavaRDD<JsonNode> records = srcRDD.map((Function<String, JsonNode>) v1 -> objectMapper.readTree(v1));
        JavaRDD<JsonNode> filterDatas = records.filter(new Function<JsonNode, Boolean>() {
            @Override
            public Boolean call(JsonNode v1) throws Exception {
                String s = v1.get("_source").get("log").asText();
                JsonNode vhost;
                try {
                    vhost=objectMapper.readTree(s).get("vhost");
                }catch ( Exception e){
                    System.out.println(s);
                    return false;
                }

                if(vhost==null){
                    return false;
                }
                return vhost.asText().equals(AnalysisForBatch.vhost);
            }
        });
        JavaRDD<JsonNode> jsonNodeJavaRDD = filterDatas.mapPartitions(new FlatMapFunction<Iterator<JsonNode>, JsonNode>() {
            StringBuffer resultBuffer = new StringBuffer();

            @Override
            public Iterator<JsonNode> call(Iterator<JsonNode> jsonNodeIterator) throws Exception {
                while (jsonNodeIterator.hasNext()) {
                    JsonNode record = jsonNodeIterator.next();
                    String indexstr = "{\"index\":{\"_index\":\"" + record.get("_index").getTextValue()+ "\",\"_type\":\"" + record.get("_type").getTextValue() + "\""+ ",\"_id\":\"" + record.get("_id").getTextValue() + "\"}";
                    String source = objectMapper.writeValueAsString(record.get("_source"));
                    resultBuffer.append(indexstr + "\n" + source + "\n");

                }
                if(resultBuffer.toString()!=""){
                   sendData(resultBuffer.toString());
                }
                return jsonNodeIterator;
            }
        });
        jsonNodeJavaRDD.take(1);

    }

    public static SparkConf getSparkConf(String[] args) {
        if (sparkConf == null) {
            synchronized (AnalysisForBatch.class) {
                String ak = args[0];
                String sk = args[1];
                sparkConf = (new SparkConf()).setAppName("com.huawei.om.Analysis");
                sparkConf.set("fs.obs.access.key", ak );
                sparkConf.set("spark.hadoop.fs.secret.key", sk);

            }
        }
        return sparkConf;
    }

    public static int sendData(String data) throws NoSuchAlgorithmException, KeyManagementException, URISyntaxException, IOException {
        CloseableHttpClient httpClient = HttpClientUtils.getClient();
        CloseableHttpResponse execute = null;
        try {
            HttpPost httpPost = new HttpPost(uri);
            httpPost.setEntity(new StringEntity(data, "UTF-8"));
            httpPost.addHeader(HttpHeaders.CONTENT_TYPE, ContentType.APPLICATION_JSON.getMimeType());
            httpPost.addHeader("Authorization","Basic "+Base64.getEncoder().encodeToString((conf.getProperty("es.user")+":"+conf.get("es.passwd")).getBytes()));
            execute = httpClient.execute(httpPost);
            System.out.println(EntityUtils.toString(execute.getEntity()));
            return execute.getStatusLine().getStatusCode();
        }  catch (IOException e) {
            return execute.getStatusLine().getStatusCode();
        } finally {
            if (execute != null) {
                try {
                    EntityUtils.consume(execute.getEntity());
                } catch (IOException e) {

                }
            }
        }
    }
}
